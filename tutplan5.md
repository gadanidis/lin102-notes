# Help lab 5

## Notes from previous help labs

https://gitlab.com/gadanidis/lin102-notes/

These are messy because they're just my own personal notes that I take when
preparing the help lab, but hopefully you find them useful!

## Midterm

This Tuesday (so there will be no lecture or tutorial).
I'll try to step through today's exercises a bit quicker so there's time for
questions at the end.

## DP clarification

We will draw DPs as follows:
- Personal names and pronouns are considered determiners with no NP complement.

Trees for "Mary" and "they":

     DP       DP
     |        |
     D        D
     |        |
    Mary     they

- All noun phrases are contained within a DP, even if there is no overt
  determiner:

Tree for "water":

     DP
    /  \
    D  NP
    |  |
    Ø  N
       |
      water

## Local vs. global ambiguity

**Local ambiguity**: the sentence has only one possible reading, but one part of
the sentence is ambiguous, possibly tricking the reader (a "garden-path" sentence).

**Global ambiguity**: the full sentence is ambiguous and has two or more
possible readings.

(a) The foundation gave Trudeau's letters to his father to a museum.
- This is local ambiguity: only one possible reading. The local ambiguity
comes from "The foundation gave Trudeau's letters to his father" --- you can initially
interpret this as the foundation giving "Trudeau's letters" to his father.
But actually, when we get to the second PP, "to a museum", we see that "to his
father" is actually modifying "Trudeau's letters", not "gave".

(b) Franny painted the drawer with the brushes.
- This is global ambiguity: "with the brushes" could modify the VP "painted the
  drawer" (so Franny used the brushes to paint) or it could modify the DP "the
  drawer" (so the drawer had brushes on it, in it, or near it, or something)

(c) Squad helps dog bite victim.
- This is not a grammatical sentence normally because it's missing some
  determiners, but it's written in "headline style" which often omits
  determiners.
- This is global ambiguity: we can interpret "dog bite victim" as one DP (the
  victim of a dog bite), or we can interpret "dog bite victim" as an additional
  clause, and the squad is helping a dog bite its victim.

(d) The students found the book was written poorly.
- This is local ambiguity: it only has one possible meaning, but on an initial
  reading, we might parse "The students found the book" as a full clause, like
  the students discovered a book. But then we get to "was written poorly" and we
  see that it's a different use of "found" which refers to perceptions and takes
  a clause, not a DP, as complement.

## Drawing ambiguity

You're asked to pick a globally ambiguous sentence and a locally ambiguous
sentence above and draw two trees for each.

I'll give a pair of full example trees for (a):

(a) The foundation gave Trudeau's letters to his father to a museum.

        CP
      /    \
    C        TP
    Ø[-q]  /    \
         DP        T'
        / \     /     \
       D   NP  T        \
      the  |  [+tense]    \
            N              VP
        foundation     /   |   \
                      V   DP    PP
                    gave / \    /   \       where does the PP "to a museum"
                         D  NP  P    DP     go? the verb doesn't c-select
                  Trudeau's  |  |   /  \    another PP!
                             N  to  D   NP
                          letters   |   |
                                    his N
                                        |
                                      father

        CP
      /    \
    C        TP
    Ø[-q]  /    \
         DP        T'
        / \     /     \
       D   NP  T        \
      the  |  [+tense]    \
            N               VP                      if we attach "to his father"
                         /  |    \                  as part of the "Trudeau's letters"
                        /   |       \               DP instead, we can fit the PP
        foundation     /    |         \             "to a museum" into the VP and all
                      V    DP           PP          is well.
                    gave  /   \        /   \
                          D    NP      P    DP
                   Trudeau's  / \      |    / \
                              N  PP    to  D   NP
                         letters /  \      |   |
                                 P    DP   a   N
                                 |   /  \      |
                                 to  D   NP    museum
                                     |   |
                                    his  N
                                         |
                                       father

For (b), the main difference between your trees should be that in one, the PP
"with the brushes" is adjoined to the VP "painted the drawer", to get the full
VP "painted the drawer with the brushes" (first tree below). In the other, the PP "with the
brushes" should be adjoined to the NP "drawer" within the DP "the drawer", to
get the full DP "the drawer with the brushes" (second tree below).

"with the brushes" adjoined to VP:

              VP
          /    |    \
        V      DP      PP
        |      / \    /   \
    painted    D  NP  P     DP
               |   |  |      / \
              the  N  with   D  NP
                   |         |   |
                 drawer     the  N
                                 |
                               brushes

"with the brushes" adjoined to DP:

            VP
           /   \
         V      DP
         |      / \
     painted   D   NP
               |   / \
             the  NP   PP
                  |   /  \
                  N   P    DP
                  |   |    / \
             drawer  with D  NP
                          |   |
                          the N
                              |
                            brushes
