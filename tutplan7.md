# Help lab 7

## Notes from previous help labs

https://gitlab.com/gadanidis/lin102-notes/

These are messy because they're just my own personal notes that I take when
preparing the help lab, but hopefully you find them useful!

## Questions from last time

- are gerunds (-ing) non-finite? yes.
    - e.g. "The government was condemned [for supporting a coup]."
    - [for supporting a coup] is a non-finite clause

Assignment 2:

- triangles: it will be made more clear on future assignments that you should
  not use triangles (except for wh-phrase). we aren't adjusting the marks for
  previous assignments using triangles because we didn't remove marks
  specifically for using triangles, we only removed marks where the triangles
  covered a part of the tree that was being graded
- constituency tests: there was a question about grading leniency for not
  modifying the sentence when doing a constituency test — this is how
  constituency tests were shown in lecture. we will aim to make instructions
  clearer in future
- NP/DP: the instruction that all NPs should be contained in DPs was conveyed
  prior to the due date for the assignment

## Grading progress

- midterm currently being graded, aiming to be done by end of week
- assignment 3 currently being graded, also aiming to be done by end of week

## Review of new concepts

This will be covered at the beginning of lecture 8 as well!

### Locality

- we expect things that are related (e.g., by complementation) to be close to
  each other in a tree
- this isn't always true (e.g., in questions)
    - in "What did the goverment tell the media?" 'what' is a complement of the
      verb 'tell' but it appears very far from 'tell'
- we explain this by saying that things start out local (underlying structure),
  and then move before the sentence is actually produced (surface structure)
- Underlying structure → movement processes → surface structure
- This means we hypothesize all English sentences to look like declaratives in
  the underlying form (i.e. not questions) and we get questions by moving stuff
  around.

### Types of movement

- heads (e.g., V, T) and phrases (e.g., DP, PP) can both move
- V-to-T movement: in some languages (e.g., French) the content of V moves to T
    - how do we know it happens in French?
        - adverbs can't appear before the verb in French (but can in English)
        - *Je soigneusement mange la pomme.
        - I carefully eat the apple.
    - why does this movement happen? → the tense information needs to be
      expressed on a verb
    - why doesn't it happen in English? → affix hopping (rather than the verb in V
      moving to T to receive tense, the tense information in T moves to V)
    - neither V-to-T nor affix hopping from T occurs if there is a modal in T
- T-to-C movement
    - in questions, the auxiliary moves to the front of the sentence
    - how do we explain this? → the content of T moves to C
    - why? → the [+q] feature triggers this movement
- wh-movement
    - a wh-phrase (what, which, who, etc.) moves to specifier of CP
    - this means we need a C' level
    - you only need to draw a C' level when there is wh-movement (but you can
      draw it in all trees if you want to)
- topicalization
    - topicalized phrases are adjoined to TP

### Auxiliaries

- two auxiliaries in English: BE and HAVE
    - (modals are not considered auxiliaries here)
- auxiliaries provide information about whether an event is complete (perfect
  aspect) or in progress (imperfect aspect)
- auxiliaries are represented as VPs
- in English, auxiliaries, unlike most main verbs, move from V to T (unless
  there is already a modal in T)
- the VP that's c-selected by an auxiliary is given a special affix that hops
  down to V (similar to how tense moves down to V, but the affixes from
  auxiliaries don't have tense)
- BE and HAVE can also be lexical verbs; lexical BE undergoes V-to-T but lexical
  HAVE does not

### Do support

- we always need to either do affix-hopping or V-to-T movement:
  the tense information always has to end up in the same place as a verb
- in negation and after T-to-C movement, affix hopping can't occur
- so in order to give the tense information a verb to express itself on, we
  stick "do" in T

### Order of operations:

1. First, do affix-hopping and/or V-to-T movement, when possible/necessary
2. Next, if V has not moved to T or received tense info from T through affix
   hopping, do support occurs: add DO to T
3. T-to-C movement, if C has a [+q] feature

The order of the other operations (e.g. wh-movement) shouldn't matter.

Note that T-to-C movement only occurs in main clauses, not embedded clauses.

## Today's review exercises

I'm going to go through the exercise via the file sharing thing.


## Questions to follow up on

- topicalization tree --- triangles allowed?
- order of operations 
- V to T arrow in key (c)
