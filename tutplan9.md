# Help lab 9

## Notes from previous help labs

https://gitlab.com/gadanidis/lin102-notes/

These are messy because they're just my own personal notes that I take when
preparing the help lab, but hopefully you find them useful!

## Questions from last time

- Always write -s for present tense regardless of person, always write -ed for
  past tense as well
- Don't treat modals as past/present tense versions of each other, 'can' and
  'could' are not considered to be conjugated

## Updates

- assignment 3 and midterm grades were released today
- you should really consider going to Kaz's office hours or making an
  appointment if you feel that you're struggling in the course, this can be very
  very helpful

## Review exercises

### Entailment

What is entailment?
- When one proposition (sentence) is always true if another proposition is true

Mutual entailment?
- when two sentences entail each other

Contradiction?
- opposite of entailment --- the two propositions can't both be true at the same
  time

(1) B entails A

(2) A entails B

(3) no relation

(4) contradiction

(5) no relation

(6) A entails B
- huh?? aren't they not related?
- A is always false, and a sentence that is always false is said to entail
  every sentence
- it is impossible for A to be true when B is false (because A can never be
  true)

(7) B entails A 
- huh?? aren't they not related?
- B is always true, and a sentence that is always true is said to
  be entailed by every sentence
- it is impossible for B to be false when A is true (because B can never be
  false)

### Presupposition

What is a presupposition? What does it mean for one sentence to presuppose
another sentence?
- A presupposes B if it entails B no matter if you negate it or not

Loaded questions: "Did you ever stop sleeping in until noon every day?"
- Yes ⇒ I used to sleep in until noon every day, and I no longer do
- No ⇒ I used to sleep in until noon every day, and I still do
- In either case, the question presupposes that in the past, you slept in until
  noon every day --- only difference is whether you still do or if you stopped

What's a factive verb?
- a verb that introduces a CP that is presupposed to be true
- *regret*, *know*, *realize*
- some factive verbs can appear factive in some cases and not factive in others,
  so the best test is to try negating the sentence that might have a
  presupposition, *not* to try to figure out if the verb is factive

(8) not presupposed

(9) presupposed

(10) not presupposed

(11) presupposed

(12) not presupposed
