# Help Lab 1
## Introduction

I'm Tim, I'm a PhD student in the department of linguistics.
I'm a TA for this course and I'll be leading these help labs, which are a way
for us to review the material and for you to ask questions about the course
content.

My own field of study is sociolinguistics, which means I'm interested in the
social aspects of language: how do our identities, beliefs, and experiences
affect the ways that we use language, and vice versa?

I did all of my post-secondary studies here at UofT, so I'm familiar with the
undergraduate, masters and PhD programs in linguistics here, so if you have
questions about those feel free to email me.

**I can't answer questions by email about course content. You can ask those
questions here in our help lab, or in Kaz's office hours.**

Some other reminders:
1. Students must go to the right help lab session or else they will not be
   graded for participation
2. To complete the lecture module #1, students must submit their answers to some
   of the questions in Part B and Part C of this exercise
3. The session starts at 5 minutes after the hour

## Bb Collaborate

If you're here listening to me, then you've figured out how to get into Bb
Collaborate, which is the video chat platform we'll be using for our help labs.
Some useful features:

1. Chat (click the purple arrows at the bottom right): you can use this to ask
   questions, either type out the question or type "I have a question" and I'll
   call on you to ask it via audio.

   Please type your preferred name, the pronouns you would like me to use to
   refer to you, your major and the last food that you ate into the chat, to
   make sure it is working and so I can check for your attendance later.

2. Hand-raising feature: You can also use this to get my attention so you can
   answer a question. Please only use this if you are answering a question that
   I'm asking, otherwise please use the chat feature so I can keep track of
   questions better. You can also answer questions by typing in chat if you are
   more comfortable doing that.

3. Microphone and camera buttons: You can use these to turn your mic and camera
   on/off. Please leave your mic off when you're not talking. You can leave the
   video on or turn it off, it's up to you. If your connection is struggling it
   might help to turn off your own video.

## Part A

This question asks about descriptive vs prescriptive grammars.
You're given two sets of language data and asked how the second data set is
perceived in descriptive vs. prescriptive grammars.

First of all, what are descriptive and prescriptive grammars?

- descriptive: a set of rules _describing_ how people _actually_ talk
- prescriptive: a set of rules _prescribing_ how people _should_ talk

So with that in mind, how would you say that Data II would be viewed in these
two different grammars?

> Sample answer:
> Data II is viewed as ‘incorrect’ in prescriptive grammar since it is
> considered ‘bad grammar’ to have more than one negative expression in a
> sentence, but the same dataset is rather considered as data demonstrating an
> interesting variation of English in descriptive grammar.

## Part B

So as we learned in lecture, words are categorized into different "categories"
or "parts of speech".
For example, nouns and verbs and adjectives and adverbs...

There are two main ways we can judge what category a word belongs to.
What are they?

- meaning: what the word means
- distributional: where the word can or cannot appear, including what
  morphemes (word parts) can attach to it and where

So with that in mind, let's walk through the exercises.

(i)
1. diligent: adj, between determiner 'the' and noun 'students'
2. in: preposition, refers to spatial relation between a preceding and following
   noun
3. surely: adverb, ends in -ly and distributional evidence means it can't be an
   adjective

(ii)
4. asked: verb, has -ed morpheme, describes an action, appears in appropriate
   location for verb (after adverb, before noun)
5. friendly: between determiner 'the' and noun, describes a property of the
   following noun
6. study: follows a determiner 'his' --- how do we know 'his' is a determiner?
   we can try replacing it with 'the' (distributional criteria)

## Any remaining questions?
