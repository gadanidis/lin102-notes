# Help lab 3

## Info survey link

https://forms.gle/MojSd1nNdWz9n5Jc8

Thanks to those who filled it out already!
I will do my best to use your preferred names and pronouns but please let me
know if I make a mistake (you can message me on Quercus if you aren't
comfortable putting it directly into the chat).

## Notes from previous help labs

https://gitlab.com/gadanidis/lin102-notes/

These are messy because they're just my own personal notes that I take when
preparing the help lab, but hopefully you find them useful!

## Review exercises

### What the hell is a constituent???

Informally:

> A constituent is a syntactic string that speakers can manipulate as a single
> chunk.

Formally:

> A constituent is a set of nodes that is **exhaustively dominated** by a single
> higher node. (i.e., a constituent is all the children of a single node, and
> all of their children, and so on until the bottom of the tree)

Four tests:

1. Substitution
2. Ellipsis
3. Coordination
4. Movement
    - topicalization
    - clefting
    - pseudoclefting


### Part A

> "Lyra and Pantalaimon will find a door into another world."
>
> Question: Each of the following examples performs a constituency test on the
> sentence above. For each one identify (1) which constituency test has applied,
> (2) how it works, and (3) what you can infer from it about the structure. The
> first one is done for you.

(a) Lyra and Pantalaimon will find it. (example)

1. Substitution test
2. *it* is substituted for *a door into another world*
3. [a door into another world] seems to be a constituent

(b) Lyra and Pantalaimon will find a door into another world, won't they?

1. Ellipsis test
2. We substitute a null string for the string *find a door into another world*
3. [find a door into another world] seems to be a constituent

(c) Lyra and Pantalaimon will find a door into another world, and Lord Asriel too.

1. Ellipsis test
2. We're supposed to be able to substitute a null string for *will find a door
   into another world* (I personally don't think this is actually grammatical
   under the intended reading).
3. We can (supposedly) infer that [will find a door into another world] is a
   constituent

(d) Lyra and Pantalaimon will find a door into another world and a friend with a secret.

1. Coordination test
2. We can coordinate (link with *and* or *or*) *a door into another world* with
   *a friend with a secret*
3. [a door into another world] seems to be a constituent

(e) Lyra and Pantalaimon will find a door into another world and out of danger.

1. Coordination test
2. We can coordinate *into another world* with *out of danger*
3. [into another world] seems to be a constituent

(f) It is Lyra and Pantalaimon who will find a door into another world.

1. Movement test (clefting)
2. *Lyra and Pantalaimon* can be the focus (part that's moved earlier in the
   sentence and follows *is*) of a cleft construction
3. [Lyra and Pantalaimon] seems to be a constituent

(g) Lord Asriel said Lyra and Pantalaimon will find a door into another world,
and find a door into another world they (Lyra and Pantalaimon) will.

1. Movement (VP preposing)
2. *find a door into another world* can be topicalized
3. [find a door into another world] is a constituent

(h) What Lyra and Pantalaimon will find a door into is another world.

1. Movement test (pseudocleft)
2. *another world* can be the focus (part that follows *is*) of a pseudocleft
   construction
3. [another world] seems to be a constituent

### Part B

Based on (h)

           /\
    another   world

Based on (e) + (h)

        /\
     into \
          /\
    another world

Based on (a) + (d) + (e) + (h)

                     /\
                    /| \
                   / |  \
                  /  |   \
                a   door  \
                           \
                           /\
                        into \
                             /\
                      another   world

I'm not drawing the rest of these trees using plaintext but the idea is you keep
doing this, adding constituents based on your answers to (a-h), until you get:

          .
         / \
        /   \
       /     \
      /|\     \
     / | \     \  
    /  |  \     \
    L and P      \
                / \
             will  \
                  / \
              find   \
                     /\
                    /| \
                   / |  \
                a   door  \
                           \
                           /\
                        into \
                             /\
                      another   world
