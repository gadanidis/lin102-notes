# Help lab 8

## Notes from previous help labs

https://gitlab.com/gadanidis/lin102-notes/

These are messy because they're just my own personal notes that I take when
preparing the help lab, but hopefully you find them useful!

## Questions from last time

### Order of operations

- affix hopping for auxiliaries happens *before* T-to-C
- affix hopping for tense affixes happens *after* T-to-C

Full order:

1. Auxiliary affix hopping
2. V-to-T movement
3. Do-support (if necessary)
3. T-to-C movement
4. Tense affix hopping

I don't think the order of other operations (e.g., wh-movement) should matter.

### Triangles allowed for a topicalization tree?

We probably won't ask you to draw something that tedious.
If we do, we'll make it clear in the instructions whether you can use triangles
(and for which phrases).

## This week's review exercises

### Tree for "What are you eating?"

Underlying structure:

"you -s be+ing eat what"

(draw this on the whiteboard manually)

---

Surface structure:

1. 'ing' lowers to eat → "eat+-ing" --- auxiliary affix-hopping
2. 'be' raises to T → "be+-s [+tense]" --- V-to-T movement
3. check for do-support, no need, so no do-support
3. T raises to C → "be+-s [+tense] Ø [+q]: --- T-to-C movement
4. "what" raises to specifier of CP --- wh-movement

(draw this on the whiteboard using a different colour)

### Filler-gap structures I

What is a filler-gap structure?
- a structure that has undergone movement
- filler: the phrase/head that moved
- gap: the location that the filler moved from, and where it should be interpreted

Types of filler-gap structures:
- wh-questions (wh-word moves to spec CP)
- indirect wh-questions (wh-word moves to spec CP)
- relative clause (wh-word moves to spec CP)
- cleft (phrase moved to focus of a cleft, i.e., becomes complement of BE)

---

1. What did we pack the dishes in?
- filler: 'what'
- gap: complement of 'in'
- type: wh-question

2. What did we pack the dishes in?
- filler: 'which book'
- gap: complement of 'read'
- type: indirect wh-question

3. Franny knows the people who Hassan will bring to the party.
- filler: 'people who'
- gap: first complement of 'bring'
- type: relative clause

4. It was his suitcase that Hassan forgot last night.
- filler: 'his suitcase'
- gap: complement of 'forgot'
- type: cleft

5. Writers like people who read.
- filler: 'people who'
- gap: specifier of TP (note that the movement doesn't skip over any actual
  words)
- type: relative clause

6. It is Montreal that Hassan said Franny would like.
- filler: Montreal
- gap: complement of 'like'
- type: cleft

### Filler-gap structures II

What's the active filler strategy?
- Hypothesis that once the parser encounters a displaced filler, it tries to use
  it to fill the first possible gap it encounters.

How could this cause a garden-path effect?
- If the first possible gap is not the actual gap.
- e.g.: "Who \_ likes pizza?" → no garden path effect, first (and only) gap is
  the right gap
- "Who did Franny like \_ to see \_?" → garden path effect, the first gap is not
  the right gap

---

(a) Who did Johnny see \_ the movie with \_?
- first gap (complement of 'see') is not the right one (complement of 'with') →
  garden path

(b) Who \_ saw Johnny in \_ the garden?
- first gap (subject of 'saw') is the right gap → no garden path

(c) I don’t know what Sam will find \_ the treasure hiding in \_.
- first gap (complement of 'find') is not the right one (complement of 'in') →
  garden path

(d) Sam discovered which clue Johnny had hidden \_ the treasure next to \_.
- first gap (complement of 'find') is not the right one (complement of 'next
  to') → garden path

## Questions

- past tense of modals? 'can' and 'could'
- For TNS of “I have seen....” should it be “ø,[+tense]” or “-s,[+tense]”
