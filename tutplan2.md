# help lab 2

## Info survey
https://forms.gle/MojSd1nNdWz9n5Jc8

## (a-f)

- what is a c-selection requirement?
- (a) c-selection requirement for (1): needs V
- (b) c-selection requirement for (2): needs Adj
- (c) Does *un* have the same meaning in (1) as in (2)?
    - (1): reverse the process denoted by V
    - (2): opposite of Adj
- (d) lexical restriction on (1)?
    - only attaches to V that are trivially reversible
    - you can undo a button but not unspill a drink, e.g.
    - the reversible ones just involve doing some motion along a path
- (e) category of *un*?
    - we have no evidence for its category because of RHHR
- (f) one lexical entry or two?
    - different meanings
    - different c-selection requirements
    - different lexical restrictions


## (g-h)

(g)

| form | free/bound   | category | c-selection | lexical restriction | meaning |
|------|--------------|----------|-------------|---------------------|---------|
| un-  | bound prefix | n/a      | \_V         | trivially reversed  | undo V  |
| un-  | bound prefix | n/a      | \_A         | n/a                 | not A   |

(h)

          V
         / \
       un-  V
            |
          button


           A
          / \
        un-  A
            / \
           V   A
           |   |
        beat   -able
