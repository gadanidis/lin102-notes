# Help lab 4

## Notes from previous help labs

https://gitlab.com/gadanidis/lin102-notes/

These are messy because they're just my own personal notes that I take when
preparing the help lab, but hopefully you find them useful!

## Update on grading

Assignment 1 is being graded and should be released this week I think (this is
not a promise)

## Review exercises

Today we're looking at phrase structure.
Elements of a phrase?

- head
    - required element of a phrase
    - determines the category (e.g. N is head of NP, V is head of VP)
    - child of XP or X'
- adjunct
    - optional modifiers that attach as sibling to XP (not X)
    - child of XP
- complement
    - phrases c-selected by heads (not optional like adjuncts)
    - sibling of head (X)
- specifier
    - sibling to X'
    - for our current purposes we only have specifiers in TP
    - this is because only TP has an intermediate X' level (T')
    - (the reason we use T' is discussed on page 274 of the textbook)

### Complements vs. adjuncts (vs. specifiers)

1. BP is an adjunct (its sibling is a phrase: AP)
2. CP is a specifier (its sibling is an X': A')
3. DP is a complement (its sibling is a head: A)

### Tree drawing

Same as with word trees, I recommend starting with identifying the categories of
each word in the tree and then building upwards.
Also try to figure out whether each phrase is an adjunct (optional modifier,
sibling to XP) or complement (mandatory, sibling to X).

(a-c) are simple phrases, not full clauses.

(a) golden statue

           NP
         /   \
      AP       NP
      |        |
      A        N
    golden  statue

(b) these small keys

           DP
         /    \
        /     NP
       D     /  \
    these   AP   NP
            |    |
            A    N
          small keys

(c) proud of Mary

         AP
        /  \
       A    PP   
      /    /  \
    proud  P   NP
           of  |
               N
               Mary

(d-h) are far more complicated clausal trees.
You have to know what CP and TP are and how they're structured.
So:

- CP
    - head is C, either a complementizer (*if*, *that*, etc.) or nothing (Ø)
    - the complement of C is a TP
    - no other intermediate levels
- TP
    - head is T, which contains tense information (+tense or -tense)
    - T also contains either an auxiliary (e.g. modals like 'will') or nothing (Ø)
    - the complement of T is a VP
    - the specifier of TP is an NP
    - because TP has a specifier, TP always includes an intermediate T' level

Sample CP+TP structure:

      CP
     /  \
    C   TP
       / \
     NP   T'
         / \
        T  VP


I won't do all the trees here, but (d) is useful to look at because it maps on
to the very simple CP+TP structure above:

       CP
     /    \
    C      \
    Ø[-q]   TP 
           /  \
         NP     T'
        /    /    \
       N    T       VP
      Mary  Ø+tense  \
                      V
                    slept

We fill in C with Ø[-q], Ø because there's no complementizer and [-q] because
the clause isn't a question.
We fill in T with Ø+tense, Ø because there's no auxiliary and +tense because the
clause is tensed (specifically past tense).
NP just contains the N *Mary* and VP just contains the V *slept*.
