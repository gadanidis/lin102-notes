# Help lab 4

## Notes from previous help labs

https://gitlab.com/gadanidis/lin102-notes/

These are messy because they're just my own personal notes that I take when
preparing the help lab, but hopefully you find them useful!

## Midterm (10m)

- How was it?
- Did anything that was included on the midterm surprise you?
- Was there anything you were expecting to see that you didn't?

## Assignment 2 (10m)

Places where several people lost marks:
- general: wherever you do a constituency test, you should explicitly state
  which test you're applying, give the modified version of the sentence, and
  state what you conclude from it
- general: it's usually a good idea to do multiple constituency tests,
  especially the coordination test which is often a false positive
- B(b): you have to apply the coordination test yourself
- C(c): I gave as many marks as possible here, but the expected answer is that
  it's not a constituent — you can discover this by comparing "\*Up the fact the
  professor looked" and "Up the mountain the tourist climbed"
- E: use the title of the section as your guide — it's called "structural
  ambiguity" so your answers should be based on the sentence structure
  - this means referring to constituency, which parts of the tree a given
    modifier attaches to, etc.
- trees:
  - don't use abbreviations (triangles) unless the instructions say so!
  - all phrases have heads and all heads have phrases
  - the basic structure of a clause always includes CP and TP, then the TP
    includes the subject NP (specifier) and the predicate VP (complement)
  - when attaching modifiers, it matters whether you attach it as a complement
    or an adjunct: adjuncts, which are 'extra' modifiers, must be attached as
    sibling to XP, whereas complements are mandatory parts of the phrase and are
    sibling to X

Any other questions? Can't spend too much time on this but will try to answer
questions about the concepts if possible.

## Review Exercises - Part A (10m)

- We learned about tense in lecture 4
- Clauses that have tense are considered "finite", and clauses without tense are
  considered "non-finite"
- It's based on whether the clause denotes a specific, *finite* length of time,
  or if the time the event occurs is not expressed by the *verb* (note that the
  time could be expressed by an adjunct and it would still be a non-finite clause)
- Non-finite clauses can't stand on their own and have to be embedded within a
  finite clause
- Non-finite clauses do not always contain an overt subject.

- They will get punished for doing it.

a. Tim wishes **to get a new laptop for his work**.

b. Kelly-Ann needs **to attend a meeting this afternoon**.

c. Greg expects **his students to attend the help labs**.

d. **To draw syntax trees** will be a challenge.

e. **For them to err** can be a problem.

## Review Exercises - Part B (20m)

All the trees we've drawn so far have been for finite clauses.
How do we draw non-finite clauses?
Two key issues:

1. Where does *to* go?
    - we consider *to* to be a marker of non-finiteness, which means it's a
      marker of tenselessness
    - where does stuff related to tense go?
    - it goes in T!
    - we mark *to* with the feature [-tense]
2. What happens when there's no subject?
    - we insert PRO, an imaginary subject
    - why not just write Ø?
    - the theory is that all clauses must have a subject; marking it as null
      would indicate that there is no subject, violating the theory
    - no such theory that all clauses must have a complementizer (just that they
      need to have a head position — it's OK if it's empty)

Drawing (a):

- I'll just screen share this
- Note that PRO is considered a pronoun so we draw it as a DP with no NP in it
- Note that *to* is marked with the feature [-tense]
- Everything else is the same as normal

Practice:

- I'll split you into breakout rooms — try drawing (c) together on the
  whiteboard
- I've never used the breakout room feature hopefully it works lol
- I'll pop in to each group if you have questions about how to do anything
- (we can do more trees if there's time)
- To turn on the whiteboard:
    1. Click the purple thing with the arrows at the bottom right corner of your
       screen (to open the sidebar)
    2. Click on the "share" tab (the icon is a box with an arrow coming out of
       it)
    3. Click on Share Blank Whiteboard
- I think you should be able to do this yourselves but just in case I'll go
  through each group to help set it up if necessary

## Closing

Any questions?
I strongly recommend trying to draw the rest of the trees on your own for
practice!

## Feedback

- ask kaz about triangles
- ask kaz about not knowing to modify the thing
- ask kaz about DP and NP
- ask kaz about uploading review exercise solutions
